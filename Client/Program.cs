using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BlazorWasmWithAAdAuth.Client.Services;
using Blazored.SessionStorage;

namespace BlazorWasmWithAAdAuth.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddHttpClient("BlazorWasmWithAAdAuth.ServerAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
                .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();
            //builder.Services.AddHttpClient<HTTPClientBackendService>("BlazorWasmWithAADAuth.ServerAPI",
            //    client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
            //    .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

            //builder.Services.AddTransient<GraphCustomAuthorizationMessageHandler>();
            //builder.Services.AddHttpClient<GraphHTTPClientService>("BlazorWasmWithAADAuth.GraphAPI",
            //    client => client.BaseAddress = new Uri("https://graph.microsoft.com/"))
            //    .AddHttpMessageHandler<GraphCustomAuthorizationMessageHandler>();

            // Supply HttpClient instances that include access tokens when making requests to the server project
            builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("BlazorWasmWithAAdAuth.ServerAPI"));
            //builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("BlazorWasmWithAAdAuth.GraphAPI"));

            builder.Services.AddBlazoredSessionStorage();

            builder.Services.AddMsalAuthentication<RemoteAuthenticationState,
                CustomUserAccount>(options =>
            {
                builder.Configuration.Bind("AzureAd", options.ProviderOptions.Authentication);
                options.ProviderOptions.DefaultAccessTokenScopes.Add("1a632fd3-aeb8-45c7-8cd4-26ab402795cf/API.Access");
                options.UserOptions.RoleClaim = "role";
            }).AddAccountClaimsPrincipalFactory<RemoteAuthenticationState, CustomUserAccount,
            CustomUserFactory>();

            await builder.Build().RunAsync();
        }
    }
}
